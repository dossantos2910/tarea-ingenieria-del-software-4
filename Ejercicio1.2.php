<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Construccion de Tabla</title>
</head>
<style> 
tr:nth-child(even) {
  background: lightgreen;
}
h1,tr{
    text-align: center;
}
</style>
<body>
    <?php
    $data = [
        [
            'nombre' => 'Coca Cola',
            'cantidad' => '100',
            'precio' => 4.500,
        ],
        [
            'nombre' => 'Pepsi',
            'cantidad' => '30',
            'precio' => 4.800,
        ],
        [
            'nombre' => 'Sprite',
            'cantidad' => '20',
            'precio' => 4.500,
        ],
        [
            'nombre' => 'Guarana',
            'cantidad' => '200',
            'precio' => 4.500,
        ],
        [
            'nombre' => 'SevenUp',
            'cantidad' => '24',
            'precio' => 4.800,
        ],
        [
            'nombre' => 'Mirinda Naranja',
            'cantidad' => '56',
            'precio' => 4.800,
        ],
        [
            'nombre' => 'Mirinda Guarana',
            'cantidad' => '89',
            'precio' => 4.800,
        ],
        [
            'nombre' => 'Fanta Naranja',
            'cantidad' => '10',
            'precio' => 4.500,
        ],
        [
            'nombre' => 'Fanta Piña',
            'cantidad' => '2',
            'precio' => 4.500,
        ],
    ];
    ?>

    <html>
    <body>
        <h1>Precio de Productos</h1>
        <table style="margin-left:auto;margin-right:auto;" width="30%" border="1">
            <caption style="background-color: rgb(255,255,0);">Productos</caption>
                <tr style="background-color: rgb(102,102,102);">
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>Precio (Gs)</th>
                </tr>

            <?php

                $concat = '';

                foreach ($data as $productos) 
                {

                    $concat .= '<tr>';
                    $concat .= '<td>' . $productos['nombre'] .'</td>';
                    $concat .= '<td>' . $productos['cantidad'] .'</td>';

                    $concat .= '<td>' . number_format($productos['precio'], 3, ',','.') .'</td>';
                    $concat .= '</tr>';
                }

                echo $concat;
            ?>
    </body>
</html>
