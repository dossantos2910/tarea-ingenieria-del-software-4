<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Primer Ejercicio, Segundo Parcial">
<title>Primer Ejercicio, Segundo Parcial</title>
</head>
<body>

<?php
    function getRandomString($length = 4) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    $Array = array();

    $i = 1;

    while ($i <= 500) {
        $Array[$i] = getRandomString();
        $i++;
    }

    echo "<table>";
        echo "<tr>";
            echo "<td><b>INDICE:</b></td>";
            echo "<td><b>VALOR:</b></td>";


    foreach ($Array as $key => $value) {
        echo "<tr>";
        echo "<td>$key</td>";
        echo "<td>$value</td>";
        echo "</tr>"; 
    }

    echo "</table>";

?>

</body>
</html>