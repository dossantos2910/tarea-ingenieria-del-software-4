<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Ejercicio 2.6">
<title>Ejercicio 2.6</title>
</head>
<body>

<?php
$a= rand(99,999);
echo "El valor de a es: $a";
echo "<br>";
$b= rand(99,999);
echo "El valor de b es: $b";
echo "<br>";
$c= rand(99,999);
echo "El valor de c es: $c";
echo "<br>";

if (($a*3) > ($b+$c)) {
    echo "La expresion a*3 es mayor que la expresion b+c";
} elseif (($a*3) <= ($b+$c)) {
    echo "La expresion b+c es mayor o igual que la expresion a*3";
}
?>

</body>
</html>