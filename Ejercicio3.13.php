<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Ejercicio13</title>
</head>
<body>

<?php

$array_uno = array(
    'string_1' => 'tirika',
    'string_2' => 'leon',
    'string_3' => 'jirafa',
    'string_4' => 'tiburon',
    'string_5' => 'pato',
    'string_6' => 'vacas',
    'string_7' => 'toros',
    'string_8' => 'iguana',
    'string_9' => 'gatos',
    'string_10' => 'Perros',
);

$array_dos = array(
    'string_a' => 'tirika',
);

foreach ($array_dos as $key => $value) {
    if (array_key_exists($key, $array_uno)) {
        echo 'existe la key del array <br>';
    } else {
        echo 'no existe la key del array <br>';
    }
}

foreach ($array_dos as $key => $value) {
    if (in_array($value, $array_uno)) {
        echo 'existe el valor del array <br>';
    } else {
        echo 'no existe el valor del array <br>';
    }
}

while ($value_uno = current($array_uno)) {
    // echo key($array_uno) . '<br />';
    // echo $value_uno . '<br />';
    if (!array_key_exists(key($array_uno), $array_dos) && !in_array($value_uno, $array_dos)) {
        $array_dos[key($array_uno)] = $value_uno;
    } else {
        echo ' existe el key o valor del array <br>';
    }

    next($array_uno);
}

echo '<pre>';
print_r($array_uno);
echo '</pre>';

echo '<pre>';
print_r($array_dos);
echo '</pre>';
?>

</body>
</html>