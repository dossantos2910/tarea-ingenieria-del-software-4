<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Ejercicio14</title>
</head>
<body>
<?php

function Funcion()
{
    $strangeAssociativeArray = array(
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
    );

    $exist = false;
    foreach ($strangeAssociativeArray as $key => $value) {
        if (
            str_starts_with($key, "a") ||
            str_starts_with($key, "d") ||
            str_starts_with($key, "m") ||
            str_starts_with($key, "z")

        ) {
            $exist = true;
            print "$key => $value";
        }
    }

    if ($exist == false) {
        print "no existe ningún índice que comience con esas letras";
    }
}
 
function str_makerand($minlength = 5, $maxlength = 10, $useupper = false, $usespecial = false, $usenumbers = true)
{
    $charset = "abcdefghijklmnopqrstuvwxyz";
    if ($useupper)   $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if ($usenumbers) $charset .= "0123456789";
    if ($usespecial) $charset .= "~@#$%^*()_+-={}|][";
    if ($minlength > $maxlength) $length = mt_rand($maxlength, $minlength);
    else                         $length = mt_rand($minlength, $maxlength);
    for ($i = 0; $i < $length; $i++) $key .= $charset[(mt_rand(0, (strlen($charset) - 1)))];
    return $key;
}
Funcion();
?>

</body>
</html>


