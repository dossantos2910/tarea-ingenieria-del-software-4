<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Mi Primer Script PHP</title>
</head>
<body>

<?php

function recorrerDesdeAtras($vector)
{
    $lenghtOfVector = count($vector);

    for ($i = $lenghtOfVector; $i >= 0; $i--) {
        echo "$vector[$i] ";
    }
}

$lista = array(
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
    rand(1, 10),
);

recorrerDesdeAtras($lista);
?>

</body>
</html>