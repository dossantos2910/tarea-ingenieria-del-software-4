<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Ejercicio18</title>
</head>
<body>
<?php
    function leerArchivo($archivo){
        $gestor = fopen($archivo, "r");
        $contenido = fread($gestor, filesize($archivo));
        $visitas = (int) $contenido;
        $visitas++;
        fclose($gestor);
        return $visitas;
    }

    function actualizarVisitas($archivo,$visitas){
      
        $gestor = fopen($archivo, "w");
        fwrite($gestor, $visitas);
        fclose($gestor);
    }

    $cantidad = leerArchivo("visitas.txt");
    actualizarVisitas("visitas.txt",$cantidad);
    echo "Visitas: ".$cantidad;
?>

</body>
</html>
