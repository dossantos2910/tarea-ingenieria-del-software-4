<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Ejercicio2</title>
</head>
<body>

<?php
 $phpVersion = PHP_VERSION;
 $phpVersionId = PHP_VERSION_ID;
 $valorMaximoEnterosVersion = PHP_INT_MAX;
 $maximunNameSize = PHP_MAXPATHLEN;
 $osVersion = PHP_OS;
 $endOfLineSymbol = PHP_EOL;
 $defaultIncludePath  = DEFAULT_INCLUDE_PATH;

 echo "Versión de PHP utilizada: $phpVersion.\n"; 
 echo "El id de la versión de PHP: $phpVersionId.\n";
 echo "El valor máximo soportado para enteros para esa versión: $valorMaximoEnterosVersion.\n";
 echo "Tamaño máximo del nombre de un archivo: $maximunNameSize.\n";
 echo "Versión del Sistema Operativo: $osVersion.\n";
 echo "Símbolo correcto de 'Fin De Línea' para la plataforma en uso: $endOfLineSymbol.\n";
 echo "El include path por defecto:   $defaultIncludePath.\n";
?>

</body>
</html>
