<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Ejercicio7</title>
</head>
<body>
<?php

$palabra = 'radar';
$palabra_invertida = strrev($palabra);

if (strcmp($palabra, $palabra_invertida)==0){
    echo $palabra . " Es un palíndromo";
}else{
    echo $palabra ." No es un palíndromo";
}

?>
</body>
</html>
