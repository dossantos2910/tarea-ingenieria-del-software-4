<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Mi Primer Script PHP">
<title>Ejercicio8</title>
</head>
<body>
<?php
$filas = 3;
$columnas = 2;

function matriz($filas, $columnas) {
    $array_filas = array();    
    for ($i=0; $i < $filas; $i++) { 
        
        $array_columnas = array();
        for ($j=0; $j < $columnas; $j++) { 
            $nro = rand(1, 99);
            array_push($array_columnas, $nro);
        }
        array_push($array_filas, $array_columnas);
    }
    
    echo '<pre>';
    print_r($array_filas);
    echo '</pre>';
}

matriz($filas, $columnas);
?>

</body>
</html>
