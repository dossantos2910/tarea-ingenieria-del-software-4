<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>

    <?php 

        class vehiculo {

            private $marca;
            private $modelo;
            private $anho;
            private $automoviles = array();

            public function __construct( $marca,  $modelo,  $anho, $automoviles) {
                $this->marca = $marca;
                $this->modelo = $modelo;
                $this->anho = $anho;
                $this->automoviles = $automoviles;
            }

            public function getMarca() {
                return $this->marca;
            }

            public function getModelo() {
                return $this->modelo;
            }

            public function getAnho() {
                return $this->anho;
            }

            public function getAutomoviles() {
                return $this->automoviles;
            }
        }
    ?>

    <?php

        class automovil {

        private  $chapa;
        private  $color;
        private  $motor;

        public function __construct( $chapa,  $color,  $motor) {
            $this->chapa = $chapa;
            $this->color = $color;
            $this->motor = $motor;
        }

        public function getChapa() {
            return $this->chapa;
        }
        public function getColor() {
            return $this->color;
        }
        public function getMotor() {
            return $this->motor;
        }

    }
    ?>

    <?php

        $automovil1 = new automovil("UVVL234", "BLANCO", "1.8");
        $automovil2 = new automovil("AAER236","NEGRO","1.5");

        $vehiculos = new vehiculo("Toyota", "Auris", "2008", array($automovil1, $automovil2));

        $listaAutomoviles = $vehiculos->getAutomoviles();
        $marcaVehiculo = $vehiculos->getMarca();

        echo ("Los Chapas de los vehiculos $marcaVehiculo son: \n");
        foreach ($listaAutomoviles as $key => $value) {
            $chapaVehiculo = $value->getChapa();
            echo (" - $chapaVehiculo \n");
        }
    ?>

</body>

</html>