<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio3</title>
</head>
<body>

    <?php 

        class Book {

            private string $title;
            private $type;
            private string $editorial;
            private int $year;
            private $isbn;
            private $authors = array();

            public function __construct(string $title, $type, string $editorial, int $year, $isbn, $authors) {
                $this->title = $title;
                $this->type = $type;
                $this->editorial = $editorial;
                $this->year = $year;
                $this->isbn = $isbn;
                $this->authors = $authors;
            }

            public function getTitle() {
                return $this->title;
            }

            public function getType() {
                return $this->type;
            }

            public function getEditorial() {
                return $this->editorial;
            }

            public function getYear() {
                return $this->year;
            }

            public function getISBN() {
                return $this->isbn;
            }

            public function getAuthors() {
                return $this->authors;
            }

        }
    ?>

    <?php

        class Author {

        private string $name;
        private string $nationality;
        private $birthDate;

        public function __construct(string $name, string $nationality, $birthDate) {
            $this->name = $name;
            $this->nationality = $nationality;
            $this->birthDate = $birthDate;
        }

        public function getName() {
            return $this->name;
        }
        public function getNationality() {
            return $this->nationality;
        }
        public function getBirthDate() {
            return $this->birthDate;
        }

    }
    ?>

    <?php

        $author1 = new Author("Ramon Silva", "Paraguayo", date('d-m-Y', strtotime('25-08-2013')));
        $author2 = new Author("El Desconocido", "Paraguayo", date('d-m-Y', strtotime('13-02-1999')));
        $author3 = new Author("Augusto Roa Bastos", "Paraguayo", date('d-m-Y', strtotime('01-09-1980')));

        $book1 = new Book("Cosas en Guarani", "Yo se algo que vos no", "Yo el Supremo", 2022, "0-952635-85-7", array($author1, $author2, $author3));

        $authorsList = $book1->getAuthors();
        $bookName = $book1->getTitle();

        print("Los autores del libro $bookName  son: \n");
        foreach ($authorsList as $key => $value) {
            $authorName = $value->getName();
            print(" - $authorName \n");
        }
    ?>

</body>
</html>