<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="description" content="Final 2">
<title>Segundo Final</title>
</head>
<body>


<?php

function insertaAlumnos()
{
    try {
        $base_de_datos = new PDO('pgsql:host=localhost;dbname=finalSegunda;','postgres','chris29');

        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
        $nombre = array('Christian','Jorge','Elias','Maria','Lucia','Amira','Augustina','Luis','Tobias','Armando','Marcos','Joaquin', 'Luismi', 'Natalia', 'Raquel', 'Betania', 'Fabiana', 'Georgina', 'Julian', 'Segundo', 'Alan', 'Renato','Jose Luis');
        $apellido = array('Alonso','Almeida','Oporto','Caballero','Smeil','Paredes','Collante','Allanda','Rodriguez','Quintana', 'Dos Santos', 'Krawiec', 'Jovellanos', 'Miramar', 'Swaul', 'Torreira', 'amarco', 'Giocomano', 'elizondo', 'Montecarlo', 'Galvanis', 'Muppis', 'lonber');

            

            for ($i=0; $i < 200; $i++) { 

            
            $sql = "INSERT INTO alumnos (nombre, apellido, matricula) VALUES ('".$nombre[array_rand($nombre)]."','".$apellido[array_rand($apellido)]."','CO".$i."');";
            $base_de_datos->query($sql)->execute(); 
           
        
        }
        
        exit;
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
        
}

function insertaCursos()
{
    try {
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $cursos = array('matematica', 'informatica', 'quimica', 'biologia', 'castellano', 'trigonometria', 'fisica', 'base de datos', 'aritmetica');
        $anos = array('2019', '2020', '2021', '2022', '2023', '2018', '2017');
       
        for ($i=0; $i < 50; $i++) { 
            for ($seccion=1; $seccion < 5; $seccion++) { 
                $sql = $base_de_datos->query("INSERT INTO cursos (nombre, ano, seccion) VALUES ('".$cursos[array_rand($cursos)]."','".$anos[array_rand($anos)]."','".$seccion."');")->execute(); 
            }
        }

        exit;
    
        
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
        
}


function insertaInscriptos()
{
    try {
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $sql_alumnos = $base_de_datos->query("SELECT * FROM alumnos");
        $alumnos = $sql_alumnos->fetchAll(PDO::FETCH_OBJ);

        $sql_cursos = $base_de_datos->query("SELECT * FROM cursos");
        $cursos = $sql_cursos->fetchAll(PDO::FETCH_OBJ);

        
        
            foreach ($alumnos as $alumno) {
                foreach ($cursos as $curso) {
                    $sql = $base_de_datos->query("INSERT INTO inscripciones (curso_id, alumno_id, fecha, activo) VALUES ('".$curso->id."', '".$alumno->id."', NOW(), true);")->execute();         
                }
                
            }

        exit;
    
        
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
        
}


function cursosConMasInscriptos()
{
    try {
        
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $inscriptos = $base_de_datos->query("select count(i.alumno_id) as cant, alumno_id  , c.nombre 
        from inscripciones i 
        join cursos c on c.id = i.curso_id 
        group by i.alumno_id, c.nombre 
        order by cant asc
        limit 5;");
        $select = $inscriptos->fetchAll(PDO::FETCH_OBJ);

       
    echo "<table border='1' cellpadding='5'>";

        echo "<tr>";

        echo "<td>Curso</td>";
        echo "<td>Id Alumno</td>";
        
        echo "</tr>";
        foreach ($select as $value) {
            echo "<tr>";
            echo "<td>$value->nombre</td>";
            echo "<td>$value->alumno_id</td>";
            echo "</tr>";
        }
        echo "</table>";
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }

}
 //solo habilito uno por vez sino se cuelga la carga de los datos estaba en prueba
insertaAlumnos();
insertaCursos();
insertaInscriptos();
cursosConMasInscriptos();
?>

</body>
</html>